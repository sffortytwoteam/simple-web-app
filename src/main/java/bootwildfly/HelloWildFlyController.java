package bootwildfly;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sffortytwo.Constants;

@RestController
public class HelloWildFlyController {
    @RequestMapping("hello")
    public String sayHello(){
        return (Constants.message);
    }
}
